﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace l24_problems
{
    class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int RoleId { get; set; }
        SqlConnection connection = new SqlConnection
               ("Server=.; Database=l24_tasks; Integrated Security=true;");

        SqlCommand command = new SqlCommand();


        public void Insert(string name, int roleId)
        {
            connection.Open();
            command.Connection = connection;
            command.CommandText =
                ($"insert into [User](UserName, RoleId) values('{name}',{roleId})");
            command.CommandType = CommandType.Text;
            command.ExecuteNonQuery();
            connection.Close();

        }

        public void Update(int id, string name, int roleId)
        {
            connection.Open();
            command.Connection = connection;
            command.CommandText =
                ($"update [User] set UserName='{name}',RoleId={roleId} where UserId={id}");
            command.CommandType = CommandType.Text;
            command.ExecuteNonQuery();
            connection.Close();

        }
        public void Delete(int id)
        {
            connection.Open();
            command.Connection = connection;
            command.CommandText =
                ($"delete from [User] where UserId={id}");
            command.CommandType = CommandType.Text;
            command.ExecuteNonQuery();
            connection.Close();

        }
    }
}
