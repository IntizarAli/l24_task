﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace l24_problems
{
    class Program
    {
        static void Main(string[] args)
        {
            SqlConnection connection = new SqlConnection
                ("Server=.; Database=l24_tasks; Integrated Security=true;");
            connection.Open();

            #region task 9
            Employee employee = new Employee();
            List<Employee> employees = employee.EmployeeList();
            foreach (Employee el in employees)
            {
                Console.WriteLine(el.Id + "--" + el.Name + "--" + el.Salary);
            }
            #endregion

            #region task 10
            Console.WriteLine("Enter Employee Id:");
            int id = int.Parse(Console.ReadLine());
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = ($"select EmployeeName from Employee where EmployeeId={id}");
            string empName = command.ExecuteScalar().ToString();
            Console.WriteLine(empName);
            #endregion

            #region task 11
            command.CommandText =
                ($"select count(EmployeeName) from Employee where EmployeeName like 'B%'");
            int count = (int)command.ExecuteScalar();
            Console.WriteLine(count);
            #endregion

            #region task 16
            employee.Insert("Fazil", 2500);
            #endregion

            #region task 17
            Console.WriteLine("Delete by EmployeeId:");
            int employeeId = int.Parse(Console.ReadLine());

            command.CommandText = ($"delete from Employee where EmployeeId={employeeId}");
            command.CommandType = CommandType.Text;
            command.ExecuteNonQuery();
            #endregion

            #region task 18
            Console.WriteLine("Enter Id:");
            int empId = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter Name:");
            string employeeName = Console.ReadLine();
            Console.WriteLine("Enter Salary:");
            double employeeSalary = double.Parse(Console.ReadLine());
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = connection;
            sqlCommand.CommandText =
                ($"update Employee set EmployeeName='{employeeName}',Salary={employeeSalary} where EmployeeId={empId}");
            sqlCommand.CommandType = CommandType.Text;
            sqlCommand.ExecuteNonQuery();
            #endregion

            #region task 19
            User user = new User();
            user.Insert(name: "Irada", roleId: 8);
            user.Update(id: 10, name: "Xalis", roleId: 9);
            user.Delete(id: 10);
            #endregion


            connection.Close();
            Console.ReadKey();
        }
    }
}
