﻿using l24_problems;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace l24_problems
{
    class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Salary { get; set; }

        SqlConnection connection = new SqlConnection
               ("Server=.;Database=l24_tasks;Integrated Security=true;");
        SqlCommand command = new SqlCommand();


        public List<Employee> EmployeeList()
        {
            connection.Open();

            command.Connection = connection;
            command.CommandText = ("select * from Employee");
            SqlDataReader dataReader = command.ExecuteReader();
            List<Employee> employees = new List<Employee>();
            while (dataReader.Read())
            {

                employees.Add(
                        new Employee()
                        {
                            Id = (int)dataReader["EmployeeId"],
                            Name = dataReader["EmployeeName"].ToString(),
                            Salary = (double)dataReader["Salary"]
                        }
                        );

            }
            connection.Close();
            return employees;

        }



        public void Insert(string name, double salary)
        {
            connection.Open();
            command.Connection = connection;
            command.CommandText = ($"insert into Employee(EmployeeName,Salary) values('{name}',{salary})");
            command.CommandType = CommandType.Text;
            command.ExecuteNonQuery();
            Console.WriteLine("Insert method finished successfully!");
        }
    }
}
